<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Users;
use Swift_Mailer;
use Swift_Message;
use Twig\Environment;


class Mailer
{
    public const FROM_ADDRESS = 'godandrew54@gmail.com';

    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $twig;


    public function __construct(Swift_Mailer $mailer, Environment  $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function sendMessage(Users $user)
    {
        $mail = $this->twig->render('confirm/confirm.html.twig', ['user' => $user]);

        $message = new Swift_Message();

        $message
            ->setSubject('You have successfully registered')
            ->setFrom(self::FROM_ADDRESS)
            ->setTo($user->getEmail())
            ->setBody($mail, 'text/html');

        $this->mailer->send($message);
    }
}
